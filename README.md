# Booking System

Two companies, COKE and PEPSI, are sharing an office building. Tomorrow is COLA day (for one day), that the two companies are celebrating. They are hosting a number of business partners in the building.

The **Booking system** is a **microservice solution** shared by two companies, COKE and PEPSI, where any user can book one of the meeting rooms available for the event COLA day.

### Business rules

* COKE and PEPSI have decided to set-up a joint booking system where any user can book one of

  the 20 meeting rooms available, 10 from each company (C01, C02, … , C10 and P01, P02, …. , P10).

* COKE and PEPSI do NOT trust each other or anyone else to not take advantage of the situation.

  **Only employees of each company** or his business partners **can book a meeting room owned by his company**.



### Use cases

* Users can **book meeting rooms** by the hour (first come, first served) **owned by their company**
* Users can **cancel their own reservations**
* Users can **see hourly meeting room booked** of any of the 20 meeting rooms on the day



## Choice and focus for technical interview

My choice was to develop **only a Backend** of the Booking system for the COLA day with a **DevOps philosophy**.

I decide to prepare a **live demo** of **deploying the solution** and **run** some **end-to-end testing** during the technical interview.



My focus was on the **design and development of an API Backend** (use cases and error management) and be ready to be integrated with a front-end.

The development was done, with in mind :

* A Backend  **ready**  for **production**

* A Backend easily **Scalable** and **Resilient**

* A **generic Backend** for **any company** and **any number of company in office building**

  (more information in README.md of [booking-service](https://gitlab.com/DranS/booking-service))

* A DevOps philosophy with **Continues Delivery process**



## Architecture

The architecture choice is **microservices** and **Event Driven** oriented

![booking system architecture](./diagram/Booking-system-Architecture.png "Booking system Architecture")



#### How it's work

##### Booking a meeting room

When an employee wants to book a meeting room, he calls the front-end (simulate by the [Postman](https://www.getpostman.com/) application) which calls the [booking service](https://gitlab.com/DranS/booking-service) with a JSON like :

```json
{
	"userEmail": "ACG21CNQFM@pepsi.com",
	"meetingRoomName": "P01",
	"timeSlot": {
		"startingHour": 2,
		"endingHour": 3
	}
}
```



* The front-end call's is rooted by the Nginx reverse proxy,  based on the URI, to the right service.
  * Like `/PEPSI/book-manager/book`  root through `PEPSI booking service`
  * Like `/COKE/book-manager/book`  root through `COKE booking service`
* The booking service authenticates the user from an authentication service (like an LDAP server, etc...)
* The booking service controls that the meeting-room exist
* Verify if a booking already exists for this time slot of the meeting-room
  - **If not**, the booking is saved in database, an event `BookingUpdatedEvent` is published in the message broker with a field `bookingState:CREATED`  and an `201 HTTP` is returned
  - **If yes**, booking service will return a Conflict code  `409 HTTP`
* The event `BookingUpdatedEvent` is read by the [consulting service](//TODO) from [Kafka](http://kafka.apache.org/). The consulting service saves in his database the new booking with all his information.



##### Cancel a meeting room

When an employee wants to cancel a booking, he calls the front-end (simulate by the [Postman](https://www.getpostman.com/) application) which calls the [booking service](https://gitlab.com/DranS/booking-service) with a JSON like :

```json
{
	"userEmail": "ACG21CNQFM@pepsi.com",
	"meetingRoomName": "P01",
	"timeSlot": {
		"startingHour": 2,
		"endingHour": 3
	}
}
```



- The front-end call's is rooted by the Nginx reverse proxy,  based on the URI, to the right service.
  - Like `/PEPSI/book-manager/cancel`  root through `PEPSI booking service`
  - Like `/COKE/book-manager/cancel`  root through `COKE booking service`
- The booking service authenticates the user from an authentication service (like an LDAP server, etc...)
- The booking service controls that the meeting-room exist
- The booking service verifies if a booking already exists for this time slot of the meeting-room by this user
  - **If yes**, the booking is deleted from database, an event `BookingUpdatedEvent` is published in the message broker with a field `bookingState:DELETED`  and an `202 HTTP` is returned
  - **If not**, booking service will return an Accepted code  `202 HTTP`  (to be fault tolerant)
- The event `BookingUpdatedEvent` is read by the [consulting service](//TODO) from [Kafka](http://kafka.apache.org/). The consulting service deletes this booking from his database.



##### Get the list of meeting room booked

When an employee wants to get all bookings from a meeting-room, he calls the front-end (simulate by the [Postman](https://www.getpostman.com/) application) which calls the [consulting service](https://gitlab.com/DranS/consulting-service) with a JSON like :

```json
{
	"meetingRoomName": "P01"
}
```



- The front-end call's is rooted by the Nginx reverse proxy, based on the URI, to the right service.
  - Like `/consulting/get/booking`  root through `consulting service`
- The consulting service finds in the database all booking link to this meeting-room
- The consulting service returns a JSON body with the list of all booking for this meeting-room



#### The key decisions

The key decisions to develop this architecture are :

- With the approach of **Domain Driven Development** (DDD), we can identify only one domain : **Booking**
  - This domain contains **two entities** : a **meeting-room** and a **user**.
  - And this domain contains **one aggregate** : a **Booking** which is linked to this two entity.
- The modification (**command**) of the status of the meeting room is decorrelated from consulting (**query**) the booking status of this meeting room. Furthermore, the ratio will potentially be larger on requests to consulting booking room than requests for booking a meeting room. So, **we can dissociate these two use cases between different services to have more granularity of scalability**. We use the **Command Query Responsibility Segregation**(CQRS) principal.
- All services **communicated asynchronously** through a **message broker** to :
  - Make the system **more robust** and **fault tolerant**.
  - **Easily scale** the system.
  - **Decouple all services** between them. They do not have knowledge of each other.
  - Make the system **easier to evolve**.
- As the companies do not trust each other or anyone else, I decide **to segregate the  [booking service](https://gitlab.com/DranS/booking-service)** and deploy an instance of [booking service](https://gitlab.com/DranS/booking-service) for each company. The advantage are :
  - Each company has the **knowledge** and the **control** of the booking of **only his meeting-room**. We ensure the segregation of the data of the company.
  - Each company can **choose his authentication method** and **authenticate only his employees**.
  - From a **resilient point of view**, if one of the booking service fails, the rest of the  [booking service](https://gitlab.com/DranS/booking-service) of other companies still working.
  - From a **scalable point of view**, we can just scale those booking services that need scaling.
- From an **evolution point of view**, we can **deploy a new booking service** for other companies **without impact the running system**.



### How blockchain is relevant

The blockchain technology is relevant  for this system in place of the database of each service and the message broker.

To replace database, we have **only two smart contracts** and **a wallet address** for each booking-service or for employees link to this booking.

The **two smart contracts** are :

* One smart contract (call **Authorize**) to store the association between **wallet address**  and **meeting-room authorized to be booked**.
  * This smart contract exposes **three methods** with arguments as a  **meeting-room name/id** and a **wallet address**:
    * A method to **add a wallet address** associate to a meeting room
    * A method to **remove a wallet address** associate to a meeting room
    * A method to **verify** if a **wallet address** is authorized to book a meeting room

- One smart contract (call **Booking**) to store the **booking** (**time slot**) associate with a **meeting-room name/id** and the **wallet address** of the entity who call the smart contract. 

  - This smart contract exposes **two methods** with arguments as a  **meeting-room name/id** and a **time slot**:
    - A method to **book** a meeting room
    - A method to **cancel** a meeting room
  - For both methods, the smart contract will call (**external call**) the first smart contract to **validate** that the **wallet address is authorize** to book or cancel the meeting room give in argument.
  - Then, the smart contract stores in his data slot the link between the booking (time slot), the meeting-room and the wallet address of the caller.

  

In this case, we can imagine two scenarios.

##### The employees have NOT a wallet on the blockchain

In this scenario, only each instance of the [booking service](https://gitlab.com/DranS/booking-service) will have a wallet.

When employees **want to book** a meeting room, he will call the front-end which will call the [booking service](https://gitlab.com/DranS/booking-service) to authenticate the user out of the blockchain (like an LDAP server, etc...). Then the booking service will call to execute the **smart contract Booking** to book the meeting room. The **smart contract Booking** calls the **smart contract Authorize** to identify the booking service by his wallet address. Then the **smart contract Booking** stores in his data slot the link between the booking (time slot), the meeting-room and the wallet address of the caller. At the end, the booking service has to save in a database an identifier of this booking associated with the user.

When employees **want to cancel** a booking,  he will call the front-end which will call the [booking service](https://gitlab.com/DranS/booking-service) to authenticate the user out of the blockchain (like an LDAP server, etc...). Then the booking service will control if it's the user who booked this booking cancel request from his database. If it's true, the booking service will call to execute the **smart contract Booking** to cancel the booking of the meeting room. The **smart contract Booking** calls the **smart contract Authorize** to identify the booking service by his wallet address. Then, the **smart contract Booking** will control if it's the **wallet address of the booking service** who booked the booking cancel request. If it's true, the smart contract will set to free booking slot of this meeting room and **reset the wallet address** associated.

![Backend connect to Etherum blockchain with centralize wallet](./diagram/Booking-system-Architecture-architecture-with-Etherum.png)



##### The employees have a wallet on the blockchain

In this scenario, each employees will have a wallet.

When employees **want to book** a meeting room, he will call a front-end (**DApp**) with **his wallet address** which will call to execute the **smart contract Booking** to book the meeting room. The **smart contract Booking** calls the **smart contract Authorize** to identify the **employee** and control if he is allowed to book this meeting-room. Then the **smart contract Booking** stores in his data slot the link between the booking (time slot), the meeting-room and the wallet address of the caller.

When employees **want to cancel** a booking, he will call a front-end (**DApp**) with **his wallet address** which  will call to execute the **smart contract Booking** to cancel the booking. The **smart contract Booking** calls the **smart contract Authorize** to identify the **employee** and control if he is allowed to cancel this meeting-room.  Then, the **smart contract Booking** will control if it's the **wallet address of the employee** who booked the booking cancel request. If it's true, the smart contract will set to free booking slot of this meeting room and **reset the wallet address** associated.



![Backend connect to Etherum blockchain with DApp](./diagram/Booking-system-DApp-with-Etherum.png)



## Deploy solution

The stack for deploying and running the solution is **Docker** and **Kubernetes**

![booking system layer](./diagram/Booking-system-Layer.png "Booking system Layer")

### How deploy the solution

#### Requirement

Before deploying the solution, you need to :

* Have **Git** on your system and have access to the `git` command
* Be connected to a **Kubernetes** and have access to the `kubectl` command
* In the **Kubernetes**, be sure you **have the right to create a namespace** and you **are able to deploy in this namespace**
* In the **Kubernetes**, be sure you have **the right to deploy an ingress-controller in the  `kube-system` namespace** or you are an [ingress-controller](https://github.com/kubernetes/ingress-nginx/blob/master/deploy/mandatory.yaml) already deployed in the `kube-system` namespace



#### Prepare for deployment

To prepare for deployment, you need to clone the `booking-solution` repository on your local machine :

```shell
git clone https://gitlab.com/DranS/booking-solution.git
```



#### Deploy

To deploy the solution, go to the `kube-deploy` directory which contain all the resources to deploy the `booking-solution` in the Kubernetes and execute the `deploy.sh`.

The  `deploy.sh` contain:

```shell
kubectl create namespace booking-system

kubectl  -n booking-system apply \
-f ./db/ \
-f ./kafka/ \
-f ./consulting-service/ \
-f ./pepsi-booking-service/ \
-f ./coke-booking-service/ \
--record

# The lb.yml is the ingress-controller
kubectl apply -f lb.yml --record
```


The archetype of a resource to deploy a service is :

```shell
NAME-service/
	- deployment.yml # The deployment script of the pod with docker image name, environment variables and policy of the service (restart, scalability, pulling image, etc...)
	- service.yml 	 # The definiton of the service in the kubernetes environment link to the pods
	- ingress.yml 	 # The definition of the rooting from outside through the service (not present if the service will not be expose outside like the database)
```



## Test the solution

#### Requirement

Before testing the solution, you need to :

* Have the [Postman](https://www.getpostman.com/) application



#### Prepare for testing

To prepare for testing the solution, you need to launch the [Postman](https://www.getpostman.com/) application.

From the `booking-solution` repository, you have to `import` the testing file `Booking-solution.postman_collection.json`

![Import testing file](./diagram/Postman-import-tests.PNG)



You have also to `import` the `environment variable  ` file `Kubernetes.postman_environment.json`

![Import env file step 1](./diagram/Postman-import-environment-step-1.PNG)

![Import env file step 2](./diagram/Postman-import-environment-step-2.PNG)



When the `Kubernetes.postman_environment.json` is import, you need to change the value of the `KUB_HOST` variable by the URL/IP of your **Kubernetes**.

![Import env file step 3](./diagram/Postman-import-environment-step-3.PNG)

![Import env file step 3](./diagram/Postman-import-environment-step-4.PNG)



When all the import are done, you should have a [Postman](https://www.getpostman.com/) with all the request ready on the left side :

![Postman ready](./diagram/Postman-ready.PNG)



#### Testing

Before launch the different tests, select the `Kubernetes` environment to top-right of the [Postman](https://www.getpostman.com/).



Now, to test the solution, you need to launch the different tests of the left side, starting by the `health check` test to ensure all the services are deployed.



## Build

#### Requirement

Before building the solution, you need to :

- Have **Git** on your system and have access to the `git` command
- Have **Docker** (or a **Docker Machine**) on your system with access to the `docker` command



#### Prepare for building

To build the solution, you need to clone the `booking-solution` repository on your local machine :

```shell
git clone https://gitlab.com/DranS/booking-solution.git
cd booking-solution
git submodule update --init --force --remote
```

P.S. : If the `git submodule` doesn't work, you need to update your version of ``git`



#### Build

To build the solution, you need to execute the `build.sh` script

```shell
#!/bin/bash

SERVICES=("db" "booking-service" "consulting-service")

for service in "${SERVICES[@]}"; do

	if [ $service = "db" ]; then
      cd db && chmod +x placeholder-manager.sh && ./placeholder-manager.sh && cd ..
    fi

    cd "$service"
    echo "building service : $service"
    docker build -t "$service" .
    echo "service $service built"
    cd ..

done
```



#### Publish docker image

##### Requirement

You need to be login to your docker registry to push the image on it `docker login $YOU_REGISTRY`



##### Publish

If you want to publish the docker image build for the booking-solution, you need to execute the `publish.sh` script with 2 arguments `tag` and `registry`.

* The `tag` argument is the apply tag of all docker images
* The  `registry` argument is the repository to push all docker images


```shell
./publish.sh $tag $registry
```


For me, the command is :

```shell
./publish.sh latest drans
```



## DevOps : Continues Delivery

The DevOps process I have developed is schematize below



![DevOps process](./diagram/Diagram-DevOps process.png)



The step are :

* Developing the service in local, builds the docker image and deploy it in my local docker-machine
* When all is validated, I commit and push the new code on the origin repository (on gitlab.com)
* The `.gitlab-ci.yml` trigger a pipeline with this job :
  * **build** : Build the service
  * **test:unit** : Launch unit tests to validate non regression
  * **publish:latest** : **build the docker image** with the service inside, **tag** the docker image and publish the image to the [public Docker Hub](https://hub.docker.com/u/drans/), in `drans` repository



Then, you just have to deploy the booking solution like describe in [Deploy paragraph](#deploy).
