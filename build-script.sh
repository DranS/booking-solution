#!/bin/sh
service=$1

if [ $service = "db" ]; then
  cd db && chmod +x placeholder-manager.sh && ./placeholder-manager.sh && cd ..
fi

cd "$service"
echo "building service : $service"
docker build -t "$service" .
echo "service $service built"
cd ..
