#!/bin/bash

SERVICES=("db" "booking-service" "consulting-service")
for service in "${SERVICES[@]}"; do
	./build-script.sh $service
done
