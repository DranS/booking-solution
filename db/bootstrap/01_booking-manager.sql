-- -----------------------------------------------------
-- Schema booking-manager
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `{{ tenant_id }}-booking-manager` ;

-- -----------------------------------------------------
-- Schema booking-manager
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `{{ tenant_id }}-booking-manager` DEFAULT CHARACTER SET utf8 ;
USE `{{ tenant_id }}-booking-manager` ;

-- -----------------------------------------------------
-- Table `meeting-room`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `meeting-room` (
  `meeting_room_id` BINARY(16) NOT NULL,
  `meeting_room_name` VARCHAR(45) NOT NULL,
  `info` TEXT NULL,
  PRIMARY KEY (`meeting_room_id`));


-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` BINARY(16) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`user_id`));


-- -----------------------------------------------------
-- Table `booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `booking` (
  `booking_id` BINARY(16) NOT NULL,
  `starting_hour` INT NOT NULL,
  `ending_hour` INT NOT NULL,
  `meeting_room_id` BINARY(16) NOT NULL,
  `user_id` BINARY(16) NOT NULL,
  PRIMARY KEY (`booking_id`),
  CONSTRAINT `fk_booking_meeting-room`
    FOREIGN KEY (`meeting_room_id`)
    REFERENCES `meeting-room` (`meeting_room_id`),
  CONSTRAINT `fk_booking_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`user_id`));

CREATE INDEX `fk_booking_meeting-room_idx` ON `booking` (`meeting_room_id` ASC);
CREATE INDEX `fk_booking_user1_idx` ON `booking` (`user_id` ASC);
