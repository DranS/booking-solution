-- -----------------------------------------------------
-- Schema consulting-service
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `consulting-service` ;

-- -----------------------------------------------------
-- Schema consulting-service
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `consulting-service` DEFAULT CHARACTER SET utf8 ;
USE `consulting-service` ;

-- -----------------------------------------------------
-- Table `booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `booking` (
  `booking_id`BINARY(16) NOT NULL,
  `starting_hour` INT NOT NULL,
  `ending_hour` INT NOT NULL,
  `tenant_id` VARCHAR(45) NOT NULL,
  `meeting_room_name` CHAR(3) NOT NULL,
  `user_email` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`booking_id`));
