-- -----------------------------------------------------
-- Schema booking-manager
-- -----------------------------------------------------
USE `{{ tenant_id }}-booking-manager` ;

INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'8c281337820b4f0fa5c4a5a07c13e88e',"C01",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'7841312a74104e5596c9b4bcf3dfba93',"C02",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'ec7fe4da43654b9cbdb847f39c571ab2',"C03",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'4b1f6dcaa8de4facb8a687596cacf427',"C04",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'497404681dd74678b5bcef62e94004e5',"C05",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'35bf539154e1428da1a0e1bfc53d26a3',"C06",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'868afe4e9b0c444b95fad7bcf6c1021e',"C07",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'ee680860ca0147248889f1324d152af2',"C08",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'd9aad2ad65ba442b9563109db33db8da',"C09",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'4f5e858bef484bb68382a71328d2d92e',"C10",null);

INSERT INTO `user`(user_id, email, name) VALUES(X'c95580949ab6461598bc62766cee22e4',"DQZ71HON0L@coke.com","DQZ71HON0L");
INSERT INTO `user`(user_id, email, name) VALUES(X'b4eb4b44096e449cad11cd2641e6b572',"7M4WU4AIVG@coke.com","7M4WU4AIVG");
INSERT INTO `user`(user_id, email, name) VALUES(X'75eaf0c49c0e4cfaa343a89ab6b6dc4d',"VI80KEDPV7@coke.com","VI80KEDPV7");
INSERT INTO `user`(user_id, email, name) VALUES(X'463a81ea4f19435e9c941fad55331300',"41WGN74MCZ@coke.com","41WGN74MCZ");
INSERT INTO `user`(user_id, email, name) VALUES(X'281d538e154a45fd8347c4d5e2a6428d',"N286VHVW62@coke.com","N286VHVW62");
INSERT INTO `user`(user_id, email, name) VALUES(X'5e6c2efbe88144da83eb0cab883ad0df',"QMWU5R8SCM@coke.com","QMWU5R8SCM");
INSERT INTO `user`(user_id, email, name) VALUES(X'669a39b75c6641bea00e2f6ad6d37327',"BEF4CALZPL@coke.com","BEF4CALZPL");
INSERT INTO `user`(user_id, email, name) VALUES(X'e5abb53fce7d42768428853c4aef3322',"WTZU0Q0GF4@coke.com","WTZU0Q0GF4");
INSERT INTO `user`(user_id, email, name) VALUES(X'd8f293caf1d9426080cbbf5e934fd911',"835XTU8D1I@coke.com","835XTU8D1I");
INSERT INTO `user`(user_id, email, name) VALUES(X'5318997115904402a7a81a705c27c369',"CWO6J7BZ9D@coke.com","CWO6J7BZ9D");
