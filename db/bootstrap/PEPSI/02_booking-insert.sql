-- -----------------------------------------------------
-- Schema booking-manager
-- -----------------------------------------------------
USE `{{ tenant_id }}-booking-manager` ;

INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'4ced9da8d75749039b5f4b9efff375f9',"P01",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'bccbaebe06aa4c28b38f363b0212bb79',"P02",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'b8efeb69fe324569ba96c8a13cc085e4',"P03",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'532cf4cabfff4c10bf20f7450e9533b5',"P04",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'592672e1889c41fe8a589e13ae1063b3',"P05",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'f07977d208214beb987ab4c894d6e018',"P06",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'63d506766b674fb6ac1376128184d899',"P07",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'0125f3fe205b49b385db1fd2a3364dee',"P08",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'e1cb9ff865354c1cad9081b620502cdc',"P09",null);
INSERT INTO `meeting-room`(meeting_room_id, meeting_room_name, info) VALUES(X'ecf7e3c92d554235ab0274456b69f9f5',"P10",null);


INSERT INTO `user`(user_id, email, name) VALUES(X'f8c6df8c8f4c404992b5b2a15b3541e3',"JPAP0NIQ9Z@pepsi.com","JPAP0NIQ9Z");
INSERT INTO `user`(user_id, email, name) VALUES(X'1c61966847e2430aa9e75a9d2439841e',"ILV67PLL7B@pepsi.com","ILV67PLL7B");
INSERT INTO `user`(user_id, email, name) VALUES(X'538b4c3a4ea0446e90d580b2ee34e7d3',"ACG21CNQFM@pepsi.com","ACG21CNQFM");
INSERT INTO `user`(user_id, email, name) VALUES(X'dcf0f44ae7434d1f8e11d0365a9fa8b9',"0N1FRW5IIK@pepsi.com","0N1FRW5IIK");
INSERT INTO `user`(user_id, email, name) VALUES(X'1db3d5ec8b2d4c0a9dc221e70dff9930',"1J87G07896@pepsi.com","1J87G07896");
INSERT INTO `user`(user_id, email, name) VALUES(X'5bbfb7f947de4865a98347bbb21e71ac',"V5IRNR8ZPH@pepsi.com","V5IRNR8ZPH");
INSERT INTO `user`(user_id, email, name) VALUES(X'fd64ca875bb34da79a2917aa53652b78',"T2P83BUSWT@pepsi.com","T2P83BUSWT");
INSERT INTO `user`(user_id, email, name) VALUES(X'6cc2587110514b3f8522638e908319c9',"3RT2QVACIH@pepsi.com","3RT2QVACIH");
INSERT INTO `user`(user_id, email, name) VALUES(X'7e11929b311240ba8400f8dcecf404a1',"DFXCJ0HQBD@pepsi.com","DFXCJ0HQBD");
INSERT INTO `user`(user_id, email, name) VALUES(X'798e1f2c9668483b98e1c49e233d09b8',"BXBA445TY3@pepsi.com","BXBA445TY3");
