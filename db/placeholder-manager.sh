#!/bin/bash
rm -Rf delivery
mkdir delivery

#=========== GLOBAL PLACEHOLDERS ==============#
TENANT_LIST=`find bootstrap/* -type d -exec basename {} \;`
echo "======================================================================="
echo "TENANT_LIST $TENANT_LIST"

for tenantId in $TENANT_LIST
do
  FILE_TO_APPLY=`find bootstrap bootstrap/$tenantId -maxdepth 1 -name "*.sql"`

  echo "working directory $FILE_TO_APPLY"

  for path in $FILE_TO_APPLY
  do
    echo "path $path"
    base_file=`basename $path`
    file="$tenantId-$base_file"
    echo "file $file"
    `sed -e "s;{{ tenant_id }};$tenantId;g" ./$path > ./delivery/$file`
  done
done


#echo "Sed command = " $sedCommand
