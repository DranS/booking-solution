#!/bin/bash

kubectl create namespace booking-system

kubectl  -n booking-system apply \
-f ./db/ \
-f ./kafka/ \
-f ./consulting-service/ \
-f ./pepsi-booking-service/ \
-f ./coke-booking-service/ \
--record

kubectl apply -f lb.yml --record
