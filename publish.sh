#!/bin/bash

if [ $# -eq 0 ]; then
	tag=latest
	registry=drans
else
	tag=$1
	registry=$2
fi


SERVICES=("db" "booking-service" "consulting-service")
for service in "${SERVICES[@]}"; do
	./build-push-script.sh $service $tag $registry
done
